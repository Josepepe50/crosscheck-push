var gcm = require('node-gcm');
var apn = require('apn');

var senderEventos = new gcm.Sender('AIzaSyD7Eh9eqFdz6mv0drdc1wDJcXhUzQ8OrhE'); //API ID 724806899127
var sender = new gcm.Sender('AIzaSyBmDFFpoUySMhCIKnahOC80Xk87ptTZiNQ'); //API ID 219185739831


var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());

/*
 curl -X POST "http://localhost:9999/ios/safety" \
 -H "Content-Type: application/json" \
 -d "{\"data\":\"Nueva Alerta\", \"ids\" : \"f91e56be47a6a6329753df788f96d7c368fdf40e8e7a0dca02292042efda1c3c\"}" \
 */
app.post("/ios/:client", function(req, res) {
    var data = {
        message: (req.body.data && req.body.data.message) ? req.body.data.message : req.body.data
    };
    var ids = req.body.ids;
    //console.log(req.body);
    if (!data || !ids) return res.status(500).json({
        status: false,
        error: 1
    });

    var options = null;

    if (req.params.client === "crosscheck") {
        ////SET NODE_ENV=production
        var root = process.cwd();
        console.log('root', root)
        options = {
            /*cert: root + '/ios_prod/cert.pem',
            key: root + '/ios_prod/key.pem',*/
            cert: root + '/ios_dev/empresa/cert.pem',
            key: root + '/ios_dev/empresa/key.pem',
            passphrase: '',
            gateway: 'gateway.push.apple.com',
            //gateway: 'gateway.sandbox.push.apple.com',
            port: 2195,
            enhanced: true,
            errorCallback: function(err, notification) {
                console.log("ERROR", err, notification);
            },
            cacheLength: 100
        };
    }
    if (!options) return res.status(500).json({
        status: false,
        error: 2
    });

    console.log("CROSSCHECK IOS", options);

    var apnConnection = new apn.Connection(options);

    apnConnection.on('connected', function() {
        console.log("Connected");
    });
    apnConnection.on('transmitted', function(note, device) {
        console.log("Notification transmitted to:" + device.token.toString('hex'));
    });

    //Errores
    apnConnection.on('transmissionError', function(errCode, note, device) {
        console.error("Notification caused error: " + errCode + " for device ", device, note);
        var msg = 'Transmision Error!';
        if (errCode == 8) {
            msg = "A error code of 8 indicates that the device token is invalid. This could be for a number of reasons - are you using the correct environment? i.e. Production vs. Sandbox";
        }
        console.log(msg);
    });

    apnConnection.on('timeout', function() {
        var msg = "Connection Timeout";
        console.log(msg);
    });
    apnConnection.on('disconnected', function() {
        var msg = "Disconnected from APNS";
        console.log(msg);
    });

    apnConnection.on('socketError', function() {
        var msg = 'adios mundo cruel!!!\n problema con socket certificados apple, revise salida a puertos 2195 y 2196';
        console.log(msg);
    });


    //var myDevice = new apn.Device(ids); // '82e312b589c85bb9cb4d98a381c47006e179c8533c489a3cc4cf13c8ed02ac91');

    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    //note.badge = 3;
    note.sound = "ping.aiff";
    note.alert = data.message; // "\uD83D\uDCE7 \u2709 You have a new message";
    note.payload = {
        'messageFrom': 'CrosscheckApp'
    };

    for (var i = 0; i < ids.length; i++) {
        apnConnection.pushNotification(note, ids[i]);
    }
    return res.status(200).json({
        status: true
    });


});

//app.post("/feedback", function(req, res) {
var options = {
    "batchFeedback": true,
    "interval": 300
};

var feedback = new apn.Feedback(options);
feedback.on("feedback", function(devices) {
    devices.forEach(function(item) {
        console.log(item);
        // Do something with item.device and item.time;
    });
});

//    res.json({status:true});
//
//});


app.post("/simple", function(req, res) {
    var data = {
        title: (req.body.data && req.body.data.title) ? req.body.data.title : 'Su empresa informa',
        message: (req.body.data && req.body.data.message) ? req.body.data.message : req.body.data
    }
    var ids = req.body.ids;
    console.log('DATA', data, 'ids', ids);
    if (!data || !ids) return res.status(500).json({
        status: false
    });
    var message = new gcm.Message({
        collapseKey: 'message',
        delayWhileIdle: false,
        data: data
    });
    sender.send(message, ids, function(err, result) {
        console.log('RESULTADO', result, 'ERROR', err);
        if (err) return res.status(500).json({
            status: false,
            message: err
        });
        res.status(200).json({
            status: true
        });
    });
});

/*app.get('/demo', function(req, res){
  
    var message = new gcm.Message({
        collapseKey: 'message',
        delayWhileIdle: false,
        data:{ "message" : "placeholder" }
    });

    var registrationIds = [];
    registrationIds.push(
// "APA91bH9VldrTrriX0K533uQNx65BigzrOTweatOwULwrMJ8Z6fiYglaVMSvjlNTe8lEdJoyWjXq3VdOFBByy8K6OeWlnQT3qymdPiAn5S2yemk5oIUN1i5879rI2M9tNiU4o7Nw8jQVKGVjiWINYHkS6elF8fZPHrIh8CvwQnjZlmgbIff-Qe4");

"Crosscheck ID APA91*");

    sender.send(message, registrationIds, 4, function (err, result) {
        console.log(result);
        if (err) return res.status(200).json({status:false, message:err});
        res.status(200).json({status:true});
    });

});*/


app.listen(9999);
console.log('Ejecutando');